# GitLab Release Process

This repository contains instructions for releasing new versions of
GitLab Community Edition (CE) and Enterprise Edition (EE).

The goal is to provide clear instructions and procedures for our entire release
process, to help anyone perform the role of [Release
Manager](general/release-manager.md).

## Quick Start

## Guides

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
